﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;
using Microsoft.Bot.Connector;
using System.Threading;
using Microsoft.Bot.Sample.QnABot;
using Microsoft.Azure.CognitiveServices.Search.CustomSearch;
using Microsoft.Azure.CognitiveServices.Search.CustomSearch.Models;
using System.Linq;
using System.Web;


namespace Scorable.Dialogs
{
    [Serializable]
    [LuisModel("6003144d-f836-40ee-bdb2-b8a56d48576c", "ebb62a542d4148f9b72668b7f83366db", domain: "westus.api.cognitive.microsoft.com")]
    public class LuisDialog : LuisDialog<object>
    {

        [LuisIntent("greeting")]
        public async Task Greeting(IDialogContext context, LuisResult result)
        {
            string message = $"Hello there";

            await context.PostAsync(message);

            context.Wait(this.MessageReceived);
        }

        private ResumeAfter<object> after()
        {
            return null;
        }

        [LuisIntent("None")]
        [LuisIntent("")]
        public async Task QnA(IDialogContext context, LuisResult result)
        {
            var userQuestion = (context.Activity as Activity).Text;
            await context.Forward(new BasicQnAMakerDialog(), ResumeAfterQnA, context.Activity, CancellationToken.None);
        }

        [LuisIntent("bing.search")]
        public async Task Bing(IDialogContext context, LuisResult result)
        {
            string search = ExtractSearchString(context, result);

            await context.PostAsync("I'm looking for " + search);

            await context.PostAsync(BingSearch.Search(search));

            await context.PostAsync(ZooplusSearchUrl(search));

            context.Wait(this.MessageReceived);
        }

        [LuisIntent("tracking")]
        public async Task Tracking(IDialogContext context, LuisResult result)
        {
            var trackingNumber = result.Entities.First().Entity;
            if (trackingNumber == "666")
            {
                await context.PostAsync("Your package is in the Hel...");
            } else
            {
                await context.PostAsync("Your package has been dispatched");
                await context.PostAsync(string.Format("http://www.dpd.co.uk/apps/tracking/?reference={0}", trackingNumber));
            }
        }

        private async Task ResumeAfterQnA(IDialogContext context, IAwaitable<object> result)
        {
            context.Done<object>(null);
        }

        private async Task ResumeAfterJokeDialog(IDialogContext context, IAwaitable<object> result)
        {
            context.Done<object>(null);
        }

        private readonly string[] searchStringPrefixes = new string[] { "find ", "search for ", "search ", "i'm looking for ", "i am looking for"};

        private string ExtractSearchString(IDialogContext context, LuisResult luisResult)
        {
            string search = "";
            if (luisResult.Entities.Count > 0)
            {
                search = luisResult.Entities.First().Entity;
            } else
            {
                foreach (string prefix in searchStringPrefixes)
                {
                    if (luisResult.Query.StartsWith(prefix))
                    {
                        search = luisResult.Query.Substring(prefix.Length);
                        break;
                    }
                }
                search = luisResult.Query;
            }
            
            return search;
        }

        private string ZooplusSearchUrl(string search)
        {
            return string.Format("Or try http://www.zooplus.com/esearch.htm#q={0}", HttpUtility.UrlPathEncode(search));
        }
    }
    
    public class BingSearch
    {
        public static string Search(string input)
        {
            var searchApi = new CustomSearchAPI(new ApiKeyServiceClientCredentials("3927225bec0f4fb5915ecb78b7115f14"));

            string result = "Try looking at \n";

            try
            {
                var webData = searchApi.CustomInstance.SearchAsync(customConfig: 1442561910, query: input, count: 2, market: "en-GB", offset: 1).Result;

                //WebPages
                if (webData?.WebPages?.Value?.Count > 0)
                {
                    result += webData.WebPages.Value[0].Url + " or " + webData.WebPages.Value[1].Url;
                }
            } catch (Exception e)
            {
                result = e.ToString();
            }
           
            return result;
        }

    }
}

